import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

function Header({toggleLanguage, language}) {
  const { t } = useTranslation();
  return (
		<header className="header-section">
			<div className="container">
				<div className="row">
					<div className="col-lg-4 col-md-3">
						<div className="logo">
							<h2 className="site-logo">Bart - UnbelievableFlavour</h2>
						</div>
					</div>
					<div className="col-lg-8 col-md-9">
						{/*<a href="" className="site-btn header-btn">Get in touch</a>*/}
						<button className="site-btn header-btn toggle-dice-menu-btn" onClick={() => toggleLanguage()}>{language}</button>
						<nav className="main-menu">
							<ul>
								<li>
									<Link to="/">{t('gallery.title')}</Link>
								</li>
								<li>
									<Link to="/about">{t('about.title')}</Link>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>
  );
}

export default Header;
