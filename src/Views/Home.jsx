import React, {useState} from "react";
import './Home.css';
import TitleSection from "../Components/TitleSection";
import GalleryImage from "../GalleryImage";
import galleryData from "../Data/galleryData.json";
import categories from "../Data/categoriesData.json";
import { useTranslation } from "react-i18next";

function Home() {
  const { t } = useTranslation();
  const [ category, setCategory] = useState("all");

  let results = galleryData;

  if(category !== "all"){
    results = galleryData.filter(item => item.categories.includes(category));
  }

  let i = 0;
  return (
    <div className="Home">
      <TitleSection name="gallery" />
      <section className="portfolio-section">
        <div className="container">
          <ul className="portfolio-filter controls">
            {categories.map(category => (
              <li>
                <li onClick={()=> setCategory(category.id)}>{t(`categories.${category.id}`)}</li>
              </li>
            ))}
          </ul>
          <p>{t('gallery.current_cat')}: <b>{t(`categories.${category}`)}</b></p>
        </div>
        <div className="container p-md-0 ">
          <div className="row portfolios-area">
            {results.map((item)=>{
              i++;
              if(i > 6){
                i = 1;
              }

              return <GalleryImage itemIndex={i} item={item} />;
            })}
          </div>
        </div>
      </section>
    </div>
  );
}

export default Home;
