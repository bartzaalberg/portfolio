import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import TitleSection from "../Components/TitleSection";

function About() {
	const { t } = useTranslation();
	const [selectedTab, setSelectedTab] = useState(1);

	const selectTab = (e,tabIndex) => {
		e.preventDefault();
		setSelectedTab(tabIndex);
	}

  return (
	<>
		<TitleSection name="about" />
		<section className="page-section">
		<div className="container">
			<div className="row">
				<div className="col-lg-6">
						<div className="tab-element">
							<div className="tab-pane" dangerouslySetInnerHTML={{__html: t('about.description')}} />
							<ul className="nav nav-tabs">
								<li className="nav-item">
									<btn className={`nav-link ${selectedTab === 1 ? "active": ""}`} href="#" onClick={(e) => selectTab(e, 1)}>About me</btn>
								</li>
								<li className="nav-item">
									<btn className={`nav-link ${selectedTab === 2 ? "active": ""}`} href="#" onClick={(e) => selectTab(e,2)}>Current job</btn>
								</li>
							</ul>
							<div className="tab-content">
								{selectedTab === 1 && (
									<div className="tab-pane show active" dangerouslySetInnerHTML={{__html: t('about.about_me')}} />
								)}
								{selectedTab === 2 && (
									<div className="tab-pane show active" dangerouslySetInnerHTML={{__html: t('about.current_job')}} />
								)}
							</div>
						</div>
					</div>
				<div className="col-lg-5 offset-lg-1">
					<figure className="pic-frame">
						<img src={process.env.PUBLIC_URL + '/img/about.jpg'} alt=""/>
					</figure>
				</div>
			</div>
		</div>
	</section>
	</>
  );
}

export default About;
