import React from "react";
import { useTranslation } from "react-i18next";

function TitleSection({name}) {
  const { t } = useTranslation();

  return (
		<section className="intro-section">
			<div className="container text-center">
				<div className="row">
					<div className="col-xl-10 offset-xl-1">
						<h2 className="section-title">{t(`${name}.title`)}</h2>
					</div>
				</div>
			</div>
		</section>
  );
}

export default TitleSection;
